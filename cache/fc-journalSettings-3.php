<?php return array (
  'authorSelfArchivePolicy' => 
  array (
    'en_US' => 'This journal permits and encourages authors to post items submitted to the journal on personal websites or institutional repositories both prior to and after publication, while providing bibliographic details that credit, if applicable, its publication in this journal.',
  ),
  'authorGuidelines' => 
  array (
    'en_US' => '<p id="docs-internal-guid-02cf10ef-c0f2-faa4-b4c1-da68e83815c5" style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: bold; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">Submitting Your Work:</span></p><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">We welcome submission of scholarly articles for peer review, as well as book reviews, reflections, interviews, response essays, and other selected non-peer reviewed work.</span></p><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">Please submit all text submissions as Microsoft Word (.doc, .docx) or Rich Text Format (.rtf) attachments to the editor through the online submission system. We also encourage multimedia texts, including podcasts, video compositions, and other creative works.</span></p><br /><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">The primary mode of submitting your work is to navigate to </span><a style="text-decoration: none;" href="/about/submissions"><span style="font-size: 15px; font-family: Arial; color: #1155cc; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: underline; vertical-align: baseline;">http://spectrajournal.org/about/submissions</span></a><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">.  You will need to create a username and password to access our online submission system.  After upload, you will be able to follow the progress of your submission at any time by logging into the platform.</span></p><br /><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: bold; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">Submission of manuscripts for peer review</span></p><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">All manuscripts must be submitted in a Word-compatible format and use Chicago style citations (see below for additional submission guidelines).</span></p><br /><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: bold; font-style: italic; font-variant: normal; text-decoration: none; vertical-align: baseline;">Instructions for Attributed Files</span></p><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">On the cover page include (a) complete contact information for all contributing authors including graduate program, year in program, and email address (b) title, key words and abstract and (c) a brief statement as to how the manuscript contributes to the issue theme and is of interest to a broad audience (d) indication of interest in participating in a short interview describing your work for posting on the journal website.</span></p><br /><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: bold; font-style: italic; font-variant: normal; text-decoration: none; vertical-align: baseline;">Instructions for Anonymous Files</span></p><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">Please submit a complete version of the manuscript with no author names, addresses, or other identifiers to the editor through the online submission system.</span></p><br /><br /><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: bold; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">Manuscript preparation (all types including non-peer reviewed pieces)</span></p><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: italic; font-variant: normal; text-decoration: none; vertical-align: baseline;">SPECTRA</span><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;"> uses Chicago citation style. For additional formatting information consult the </span><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: italic; font-variant: normal; text-decoration: none; vertical-align: baseline;">Chicago Manual of Style</span><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: italic; font-variant: normal; text-decoration: none; vertical-align: baseline;">16th</span><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: italic; font-variant: normal; text-decoration: none; vertical-align: baseline;">Edition</span><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">. All references and citations should follow the Chicago guidelines.</span></p><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">Manuscripts of </span><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: bold; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">5,000 words or less </span><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">(unless by permission of the editors) should be double-spaced, including references, and on 8 ½  x 11 paper using one inch margins.</span></p><br /><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: bold; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">Book Reviews</span></p><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: italic; font-variant: normal; text-decoration: none; vertical-align: baseline;">SPECTRA </span><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;"> welcomes book reviews. If you are interested in writing book reviews, please email the editor. The editor will add your name to a roster for upcoming reviews as books become available. Please let the editor know if you are interested in reviewing a particular book, and the journal will try to acquire a copy; however, this may not always be possible.</span></p><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">Book reviews may focus on a single work or several related works. Submissions should consider the purpose of the journal in book selection: a book review is not simply a summary and critique of a book, but views the book within the context of the larger academic discipline.  Appropriate book reviews should consider the background of the book’s author, the soundness of the argument and methods, and how the book fits into the larger academic debates raised within </span><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: italic; font-variant: normal; text-decoration: none; vertical-align: baseline;">SPECTRA</span><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">.</span></p><br /><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">At the beginning of your review, please include the following information for the book(s) being reviewed:</span></p><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">●     Full citation in Chicago format</span></p><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">●     Hardback/Paperback</span></p><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">●     Cost</span></p><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">●     ISBN number</span></p><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">●     Title of the review</span></p><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">●     On average, book reviews should be approximately 1,500-2,000 words.</span></p><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">●     Submissions related to the theme of the edition are preferred, but not required</span></p><br /><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: bold; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">Multimedia Content</span></p><p style="line-height: 1.368; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: italic; font-variant: normal; text-decoration: none; vertical-align: baseline;">SPECTRA</span><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;"> accepts a wide range of original multimedia pieces for publication. These works include, but are not limited to, podcasts, digital videos, internet-hosted texts, artwork, comics, and photography. You must own all music, images, etc. or have permission for any copyright materials included in the piece. Additionally, Multimedia pieces should relate to an issue’s theme.</span></p><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">If you have a multimedia piece that you would like to have considered for publication in </span><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: italic; font-variant: normal; text-decoration: none; vertical-align: baseline;">SPECTRA</span><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">, please email the editor. In your email, please include (a) complete contact information including graduate program, year in program, and email address, (b) the title, file format, and file size of your piece, and (c) a brief statement indicating how the piece relates to the issue’s theme.</span>',
  ),
  'onlineIssn' => '21628793',
  'mailingAddress' => '',
  'useEditorialBoard' => false,
  'contactName' => 'Sascha Engel',
  'contactEmail' => 'editor@spectrajournal.org',
  'contactPhone' => '',
  'contactFax' => '',
  'supportName' => 'Claudio D\'Amato',
  'supportEmail' => 'claudio1@vt.edu',
  'supportPhone' => '',
  'sponsors' => 
  array (
  ),
  'emailSignature' => '________________________________________________________________________
SPECTRA
http://ejournals.lib.vt.edu/SPECTRA',
  'printIssn' => '',
  'numWeeksPerReview' => 2,
  'publisherUrl' => '',
  'contributors' => 
  array (
  ),
  'supportedLocales' => 
  array (
    0 => 'en_US',
  ),
  'supportedFormLocales' => 
  array (
    0 => 'en_US',
  ),
  'supportedSubmissionLocales' => 
  array (
    0 => 'en_US',
  ),
  'envelopeSender' => '',
  'publisherInstitution' => '',
  'rtAbstract' => true,
  'rtCaptureCite' => true,
  'rtViewMetadata' => true,
  'rtSupplementaryFiles' => true,
  'rtPrinterFriendly' => true,
  'rtAuthorBio' => true,
  'rtDefineTerms' => true,
  'rtAddComment' => true,
  'rtEmailAuthor' => true,
  'rtEmailOthers' => true,
  'submissionFee' => 0,
  'submissionFeeName' => 
  array (
    'en_US' => 'Article Submission',
  ),
  'submissionFeeDescription' => 
  array (
    'en_US' => 'Authors are required to pay an Article Submission Fee as part of the submission process to contribute to review costs.',
  ),
  'fastTrackFee' => 0,
  'fastTrackFeeName' => 
  array (
    'en_US' => 'Fast-Track Review',
  ),
  'fastTrackFeeDescription' => 
  array (
    'en_US' => 'With the payment of this fee, the review, editorial decision, and author notification on this manuscript is guaranteed to take place within 4 weeks.',
  ),
  'publicationFee' => 0,
  'publicationFeeName' => 
  array (
    'en_US' => 'Article Publication',
  ),
  'publicationFeeDescription' => 
  array (
    'en_US' => 'If this paper is accepted for publication, you will be asked to pay an Article Publication Fee to cover publications costs.',
  ),
  'waiverPolicy' => 
  array (
    'en_US' => 'If you do not have funds to pay such fees, you will have an opportunity to waive each fee. We do not want fees to prevent the publication of worthy work.',
  ),
  'purchaseArticleFee' => 0,
  'purchaseArticleFeeName' => 
  array (
    'en_US' => 'Purchase Article',
  ),
  'allowRegReviewer' => true,
  'purchaseArticleFeeDescription' => 
  array (
    'en_US' => 'The payment of this fee will enable you to view, download, and print this article.',
  ),
  'membershipFee' => 0,
  'membershipFeeName' => 
  array (
    'en_US' => 'Association Membership',
  ),
  'membershipFeeDescription' => 
  array (
    'en_US' => 'The payment of this fee will enroll you as a member in this association for one year and provide you with free access to this journal.',
  ),
  'donationFeeName' => 
  array (
    'en_US' => 'Donations to journal',
  ),
  'donationFeeDescription' => 
  array (
    'en_US' => 'Donations of any amount to this journal are gratefully received and provide a means for the editors to continue to provide a journal of the highest quality to its readers.',
  ),
  'metaCitations' => true,
  'allowRegAuthor' => true,
  'copySubmissionAckAddress' => '',
  'requireAuthorCompetingInterests' => false,
  'requireReviewerCompetingInterests' => false,
  'metaDiscipline' => false,
  'metaSubjectClass' => false,
  'metaSubject' => true,
  'metaCoverage' => false,
  'metaType' => false,
  'remindForInvite' => false,
  'remindForSubmit' => false,
  'numDaysBeforeInviteReminder' => 0,
  'numDaysBeforeSubmitReminder' => 0,
  'rateReviewerOnQuality' => false,
  'restrictReviewerFileAccess' => true,
  'metaCitationOutputFilterId' => -1,
  'copySubmissionAckPrimaryContact' => true,
  'copySubmissionAckSpecified' => false,
  'includeCreativeCommons' => false,
  'copyrightNoticeAgree' => true,
  'showEnsuringLink' => true,
  'mailSubmissionsToReviewers' => false,
  'authorSelectsEditor' => false,
  'enableLockss' => true,
  'reviewerDatabaseLinks' => 
  array (
    0 => 
    array (
      'title' => '',
      'url' => '',
    ),
  ),
  'notifyAllAuthorsOnDecision' => true,
  'articleEventLog' => true,
  'articleEmailLog' => true,
  'initialVolume' => 1,
  'initialNumber' => 1,
  'initialYear' => 2011,
  'restrictSiteAccess' => false,
  'restrictArticleAccess' => false,
  'publicationFormatVolume' => true,
  'publicationFormatNumber' => true,
  'useCopyeditors' => true,
  'useLayoutEditors' => false,
  'provideRefLinkInstructions' => false,
  'useProofreaders' => true,
  'publishingMode' => 0,
  'publicationFormatYear' => true,
  'showGalleyLinks' => false,
  'enableAnnouncements' => true,
  'enableAnnouncementsHomepage' => false,
  'numAnnouncementsHomepage' => 0,
  'volumePerYear' => 0,
  'issuePerVolume' => 0,
  'enablePublicIssueId' => false,
  'enablePublicArticleId' => false,
  'enablePublicGalleyId' => false,
  'enablePublicSuppFileId' => false,
  'enablePageNumber' => true,
  'submissionChecklist' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'order' => '1',
        'content' => 'The submission has not been previously published. Let editor know in case published someplace else (we are open content).',
      ),
      1 => 
      array (
        'order' => '2',
        'content' => 'The text adheres to the stylistic and bibliographic requirements outlined in the <a href="/SPECTRA/about/submissions#authorGuidelines" target="_new">Author Guidelines</a>, which is found in About the Journal.',
      ),
      2 => 
      array (
        'order' => '3',
        'content' => 'If submitting to a peer-reviewed section of the journal, the instructions in <a href="javascript:openHelp(\'http://ejournals.lib.vt.edu/SPECTRA/help/view/editorial/topic/000044\')">Ensuring a Blind Review</a> have been followed.',
      ),
    ),
  ),
  'copyrightNotice' => 
  array (
    'en_US' => '<span>Authors retain copyright to their published work.</span>',
  ),
  'contactMailingAddress' => 
  array (
    'en_US' => '<p>531 Major Williams Hall (0192)</p><p>Blacksburg, VA 24061</p>',
  ),
  'searchDescription' => 
  array (
    'en_US' => 'Interdisciplinary Journal',
  ),
  'searchKeywords' => 
  array (
    'en_US' => 'interdisciplinary, student-led, academic, social sciences, humanities, history, philosophy, political science, religion, culture, anthropology',
  ),
  'homeHeaderTitleImage' => 
  array (
    'en_US' => 
    array (
      'name' => 'Logo.jpg',
      'uploadName' => 'homeHeaderTitleImage_en_US.jpg',
      'width' => 325,
      'height' => 39,
      'mimeType' => 'image/jpeg',
      'dateUploaded' => '2015-01-21 08:49:36',
    ),
  ),
  'disableUserReg' => false,
  'allowRegReader' => true,
  'publicationFormatTitle' => true,
  'includeCopyrightStatement' => true,
  'licenseURL' => '',
  'includeLicense' => false,
  'copyrightHolderType' => 'author',
  'pageHeaderTitleImage' => 
  array (
    'en_US' => 
    array (
      'name' => 'Logo.jpg',
      'uploadName' => 'pageHeaderTitleImage_en_US.jpg',
      'width' => 325,
      'height' => 39,
      'mimeType' => 'image/jpeg',
      'dateUploaded' => '2015-01-21 08:48:41',
    ),
  ),
  'focusScopeDesc' => 
  array (
    'en_US' => '<strong>Social, Political, Ethical, and Cultural Theory Archives (SPECTRA)</strong><span> is a student-led online scholarly journal established as part of the </span><a href="http://aspect.vt.edu/" target="_blank">ASPECT</a><span> (Alliance for Social, Political, Ethical, and Cultural Thought) program at Virginia Tech. The journal features work of an interdisciplinary nature and is designed to provide an academic forum for students to showcase research, explore controversial topics, and take intellectual risks. SPECTRA welcomes submissions for publication by way of scholarly refereed articles, book reviews, essays, interviews and other works that operate within a problem-centered, theory-driven framework.</span>',
  ),
  'reviewerAccessKeysEnabled' => true,
  'customAboutItems' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'title' => '',
        'content' => '',
      ),
    ),
  ),
  'lockssLicense' => 
  array (
    'en_US' => 'This journal utilizes the LOCKSS system to create a distributed archiving system among participating libraries and permits those libraries to create permanent archives of the journal for purposes of preservation and restoration. <a href="http://lockss.org/">More...</a>',
  ),
  'categories' => NULL,
  'title' => 
  array (
    'en_US' => 'SPECTRA: the Social, Political, Ethical, and Cultural Theory Archives',
  ),
  'initials' => 
  array (
    'en_US' => 'SPECTRA',
  ),
  'abbreviation' => 
  array (
    'en_US' => 'SPECTRA',
  ),
  'contactTitle' => 
  array (
    'en_US' => 'Editor',
  ),
  'contactAffiliation' => 
  array (
    'en_US' => 'ASPECT
Department of Political Science
Virginia Tech',
  ),
  'copyeditInstructions' => 
  array (
    'en_US' => 'The copyediting stage is intended to improve the flow, clarity, grammar, wording, and formatting of the article. It represents the last chance for the author to make any substantial changes to the text because the next stage is restricted to typos and formatting corrections. The file to be copyedited is in Word or .rtf format and therefore can easily be edited as a word processing document. The set of instructions displayed here proposes two approaches to copyediting. One is based on Microsoft Word\'s Track Changes feature and requires that the copy editor, editor, and author have access to this program. A second system, which is software independent, has been borrowed, with permission, from the Harvard Educational Review. The journal editor is in a position to modify these instructions, so suggestions can be made to improve the process for this journal.<h4>Copyediting Systems</h4><strong>1. Microsoft Word\'s Track Changes</strong> Under Tools in the menu bar, the feature Track Changes enables the copy editor to make insertions (text appears in color) and deletions (text appears crossed out in color or in the margins as deleted). The copy editor can posit queries to both the author (Author Queries) and to the editor (Editor Queries) by inserting these queries in square brackets. The copyedited version is then uploaded, and the editor is notified. The editor then reviews the text and notifies the author. The editor and author should leave those changes with which they are satisfied. If further changes are necessary, the editor and author can make changes to the initial insertions or deletions, as well as make new insertions or deletions elsewhere in the text. Authors and editors should respond to each of the queries addressed to them, with responses placed inside the square brackets. After the text has been reviewed by editor and author, the copy editor will make a final pass over the text accepting the changes in preparation for the layout and galley stage. <strong>2. Harvard Educational Review </strong> <strong>Instructions for Making Electronic Revisions to the Manuscript</strong> Please follow the following protocol for making electronic revisions to your manuscript: <strong>Responding to suggested changes.</strong> For each of the suggested changes that you accept, unbold the text.   For each of the suggested changes that you do not accept, re-enter the original text and <strong>bold</strong> it. <strong>Making additions and deletions.</strong> Indicate additions by <strong>bolding</strong> the new text.   Replace deleted sections with: <strong>[deleted text]</strong>.   If you delete one or more sentence, please indicate with a note, e.g., <strong>[deleted 2 sentences]</strong>. <strong>Responding to Queries to the Author (QAs).</strong> Keep all QAs intact and bolded within the text. Do not delete them.   To reply to a QA, add a comment after it. Comments should be delimited using: <strong>[Comment:]</strong> e.g., <strong>[Comment: Expanded discussion of methodology as you suggested]</strong>. <strong>Making comments.</strong> Use comments to explain organizational changes or major revisions   e.g., <strong>[Comment: Moved the above paragraph from p. 5 to p. 7].</strong>Note: When referring to page numbers, please use the page numbers from the printed copy of the manuscript that was sent to you. This is important since page numbers may change as a document is revised electronically.<h4>An Illustration of an Electronic Revision</h4><ol><li><strong>Initial copyedit.</strong> The journal copy editor will edit the text to improve flow, clarity, grammar, wording, and formatting, as well as including author queries as necessary. Once the initial edit is complete, the copy editor will upload the revised document through the journal Web site and notify the author that the edited manuscript is available for review.</li><li><strong>Author copyedit.</strong> Before making dramatic departures from the structure and organization of the edited manuscript, authors must check in with the editors who are co-chairing the piece. Authors should accept/reject any changes made during the initial copyediting, as appropriate, and respond to all author queries. When finished with the revisions, authors should rename the file from AuthorNameQA.doc to AuthorNameQAR.doc (e.g., from LeeQA.doc to LeeQAR.doc) and upload the revised document through the journal Web site as directed.</li><li><strong>Final copyedit.</strong> The journal copy editor will verify changes made by the author and incorporate the responses to the author queries to create a final manuscript. When finished, the copy editor will upload the final document through the journal Web site and alert the layout editor to complete formatting.</li></ol>',
  ),
  'refLinkInstructions' => 
  array (
    'en_US' => '<h4>To Add Reference Linking to the Layout Process</h4><p>When turning a submission into HTML or PDF, make sure that all hyperlinks in the submission are active.</p><h4>A. When the Author Provides a Link with the Reference</h4><ol><li>While the submission is still in its word processing format (e.g., Word), add the phrase VIEW ITEM to the end of the reference that has a URL.</li><li>Turn that phrase into a hyperlink by highlighting it and using Word\'s Insert Hyperlink tool and the URL prepared in #2.</li></ol><h4>B. Enabling Readers to Search Google Scholar For References</h4><ol><li>While the submission is still in its word processing format (e.g., Word), copy the title of the work referenced in the References list (if it appears to be too common a title—e.g., "Peace"—then copy author and title).</li><li>Paste the reference\'s title between the %22\'s, placing a + between each word: http://scholar.google.com/scholar?q=%22PASTE+TITLE+HERE%22&amp;hl=en&amp;lr=&amp;btnG=Search.</li><li>Add the phrase GS SEARCH to the end of each citation in the submission\'s References list.</li><li>Turn that phrase into a hyperlink by highlighting it and using Word\'s Insert Hyperlink tool and the URL prepared in #2.</li></ol><h4>C. Enabling Readers to Search for References with a DOI</h4><ol><li>While the submission is still in Word, copy a batch of references into CrossRef Text Query http://www.crossref.org/freeTextQuery/.</li><li>Paste each DOI that the Query provides in the following URL (between = and &amp;): http://www.cmaj.ca/cgi/external_ref?access_num=PASTE DOI#HERE&amp;link_type=DOI.</li><li>Add the phrase CrossRef to the end of each citation in the submission\'s References list.</li><li>Turn that phrase into a hyperlink by highlighting the phrase and using Word\'s Insert Hyperlink tool and the appropriate URL prepared in #2.</li></ol>',
  ),
  'proofInstructions' => 
  array (
    'en_US' => '<p>The proofreading stage is intended to catch any errors in the galley\'s spelling, grammar, and formatting. More substantial changes cannot be made at this stage, unless discussed with the Section Editor. In Layout, click on VIEW PROOF to see the HTML, PDF, and other available file formats used in publishing this item.</p><h4>For Spelling and Grammar Errors</h4><p>Copy the problem word or groups of words and paste them into the Proofreading Corrections box with "CHANGE-TO" instructions to the editor as follows:</p><pre>1. CHANGE...
	then the others
	TO...
	than the others</pre><br /><pre>2. CHANGE...
	Malinowsky
	TO...
	Malinowski</pre><br /><h4>For Formatting Errors</h4><p>Describe the location and nature of the problem in the Proofreading Corrections box after typing in the title "FORMATTING" as follows:</p><br /><pre>3. FORMATTING
	The numbers in Table 3 are not aligned in the third column.</pre><br /><pre>4. FORMATTING
	The paragraph that begins "This last topic..." is not indented.</pre>',
  ),
  'openAccessPolicy' => 
  array (
    'en_US' => 'This journal provides immediate open access to its content on the principle that making research freely available to the public supports a greater global exchange of knowledge.',
  ),
  'homeHeaderTitleType' => 
  array (
    'en_US' => '1',
  ),
  'pageHeaderTitleType' => 
  array (
    'en_US' => '1',
  ),
  'readerInformation' => 
  array (
    'en_US' => 'We encourage readers to sign up for the publishing notification service for this journal. Use the <a href="/user/register">Register</a> link at the top of the home page for the journal. This registration will result in the reader receiving the Table of Contents by email for each new issue of the journal. This list also allows the journal to claim a certain level of support or readership. See the journal\'s <a href="/about/submissions#privacyStatement">Privacy Statement</a>, which assures readers that their name and email address will not be used for other purposes.',
  ),
  'authorInformation' => 
  array (
    'en_US' => 'Interested in submitting to this journal? We recommend that you review the <a href="/about">About the Journal</a> page for the journal\'s section policies, as well as the <a href="/about/submissions#authorGuidelines">Author Guidelines</a>. Authors need to <a href="/user/register">register</a> with the journal prior to submitting or, if already registered, can simply <a href="/login">log in</a> and begin the five-step process.',
  ),
  'librarianInformation' => 
  array (
    'en_US' => 'We encourage research librarians to list this journal among their library\'s electronic journal holdings. As well, it may be worth noting that this journal\'s open source publishing system is suitable for libraries to host for their faculty members to use with journals they are involved in editing (see <a href="http://pkp.sfu.ca/ojs">Open Journal Systems</a>).',
  ),
  'displayCurrentIssue' => true,
  'description' => 
  array (
    'en_US' => '<p><strong>Social, Political, Ethical, and Cultural Theory Archives (SPECTRA)</strong><span> is a student-led online scholarly journal established as part of the </span><a href="http://aspect.vt.edu/" target="_blank">ASPECT</a><span> (Alliance for Social, Political, Ethical, and Cultural Thought) program at Virginia Tech. The journal features work of an interdisciplinary nature and is designed to provide an academic forum for students to showcase research, explore controversial topics, and take intellectual risks. SPECTRA welcomes submissions for publication by way of scholarly refereed articles, book reviews, essays, interviews and other works that operate within a problem-centered, theory-driven framework.</span></p>Please find our latest Call for Submissions <a href="/announcement">here</a>.',
  ),
  'navItems' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'name' => 'Home',
        'isLiteral' => '1',
        'url' => '',
      ),
      1 => 
      array (
        'name' => 'About',
        'isLiteral' => '1',
        'url' => '',
      ),
      2 => 
      array (
        'name' => 'Call for Papers',
        'isLiteral' => '1',
        'url' => '',
      ),
      3 => 
      array (
        'name' => 'Current Issue',
        'isLiteral' => '1',
        'url' => '',
      ),
      4 => 
      array (
        'name' => 'Previous Issues',
        'isLiteral' => '1',
        'url' => '',
      ),
      5 => 
      array (
        'name' => 'Submission Guidelines',
        'isLiteral' => '1',
        'url' => '',
      ),
    ),
  ),
  'itemsPerPage' => 25,
  'numPageLinks' => 10,
  'journalTheme' => 'uncommon',
); ?>