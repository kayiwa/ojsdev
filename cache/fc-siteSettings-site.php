<?php return array (
  'title' => 
  array (
    'en_US' => 'eJournal Publishing Services',
  ),
  'pageHeaderTitleType' => 
  array (
    'en_US' => '1',
  ),
  'intro' => 
  array (
    'en_US' => '<h2><em>New</em> - eJournal Publishing Services Offered by the University Libraries</h2> <p>December 7, 2012 - The Digital Library and Archives now supports the full range of journal publishing services, from article submissions and peer review, to archiving and access.</p> <p>The first journal to use the Libraries\' new service is <a href="/JRMP">Journal of Research in Music Performance</a>, edited by Kelly Parkes, Associate Professor of Music Education at Virginia Tech.</p> <p>DLA is now hosting the open source publishing platform, OJS: <a href="http://pkp.sfu.ca/?q=ojs">Open Journal Systems</a>. For more information contact: Gail McMillan, gailmac@vt.edu.</p> <p>We continue to host many other post-peer review, open access journals.</p> <h4><a href="http://scholar.lib.vt.edu/ejournals/">Complete List of DLA Ejournals</a></h4>',
  ),
  'contactName' => 
  array (
    'en_US' => 'Open Journal Systems',
  ),
  'contactEmail' => 
  array (
    'en_US' => 'ojs@dla.lib.vt.edu',
  ),
  'pageHeaderTitleImage' => 
  array (
    'en_US' => 
    array (
      'originalFilename' => 'vt_header_screen_ojs.jpg',
      'width' => 758,
      'height' => 97,
      'uploadName' => 'pageHeaderTitleImage_en_US.jpg',
      'dateUploaded' => '2012-11-20 14:23:11',
      'altText' => '',
    ),
  ),
  'showThumbnail' => true,
  'showTitle' => true,
  'showDescription' => true,
  'defaultMetricType' => 'ojs::counter',
); ?>