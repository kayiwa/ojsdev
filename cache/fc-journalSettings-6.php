<?php return array (
  'authorSelfArchivePolicy' => 
  array (
    'en_US' => 'This journal permits and encourages authors to post items submitted to the journal on personal websites or institutional repositories both prior to and after publication, while providing bibliographic details that credit, if applicable, its publication in this journal.',
  ),
  'fastTrackFeeDescription' => 
  array (
    'en_US' => 'With the payment of this fee, the review, editorial decision, and author notification on this manuscript is guaranteed to take place within 4 weeks.',
  ),
  'publicationFee' => 0,
  'publicationFeeName' => 
  array (
    'en_US' => 'Article Publication',
  ),
  'title' => 
  array (
    'en_US' => 'The Journal of Technology Studies',
  ),
  'numWeeksPerReview' => 4,
  'contributors' => 
  array (
  ),
  'itemsPerPage' => 25,
  'numPageLinks' => 10,
  'envelopeSender' => '',
  'sponsors' => 
  array (
    0 => 
    array (
      'institution' => 'Epsilon Pi Tau, The International Honor Society for Professions in Technology',
      'url' => 'http://www.epsilonpitau.org/',
    ),
  ),
  'publisherInstitution' => 'Scholarly Communication, Virginia Tech University Libraries',
  'publisherUrl' => 'http://scholar.lib.vt.edu/',
  'supportedLocales' => 
  array (
    0 => 'en_US',
  ),
  'supportedFormLocales' => 
  array (
    0 => 'en_US',
  ),
  'supportedSubmissionLocales' => 
  array (
    0 => 'en_US',
  ),
  'rtAbstract' => true,
  'rtCaptureCite' => true,
  'rtViewMetadata' => true,
  'rtSupplementaryFiles' => true,
  'rtPrinterFriendly' => true,
  'rtAuthorBio' => true,
  'rtDefineTerms' => true,
  'rtAddComment' => true,
  'rtEmailAuthor' => true,
  'rtEmailOthers' => true,
  'submissionFee' => 0,
  'submissionFeeName' => 
  array (
    'en_US' => 'Article Submission',
  ),
  'submissionFeeDescription' => 
  array (
    'en_US' => 'Authors are required to pay an Article Submission Fee as part of the submission process to contribute to review costs.',
  ),
  'fastTrackFee' => 0,
  'fastTrackFeeName' => 
  array (
    'en_US' => 'Fast-Track Review',
  ),
  'allowRegReader' => true,
  'allowRegAuthor' => true,
  'allowRegReviewer' => true,
  'publicationFeeDescription' => 
  array (
    'en_US' => 'If this paper is accepted for publication, you will be asked to pay an Article Publication Fee to cover publications costs.',
  ),
  'waiverPolicy' => 
  array (
    'en_US' => 'If you do not have funds to pay such fees, you will have an opportunity to waive each fee. We do not want fees to prevent the publication of worthy work.',
  ),
  'purchaseArticleFee' => 0,
  'purchaseArticleFeeName' => 
  array (
    'en_US' => 'Purchase Article',
  ),
  'purchaseArticleFeeDescription' => 
  array (
    'en_US' => 'The payment of this fee will enable you to view, download, and print this article.',
  ),
  'membershipFee' => 0,
  'membershipFeeName' => 
  array (
    'en_US' => 'Association Membership',
  ),
  'membershipFeeDescription' => 
  array (
    'en_US' => 'The payment of this fee will enroll you as a member in this association for one year and provide you with free access to this journal.',
  ),
  'donationFeeName' => 
  array (
    'en_US' => 'Donations to journal',
  ),
  'donationFeeDescription' => 
  array (
    'en_US' => 'Donations of any amount to this journal are gratefully received and provide a means for the editors to continue to provide a journal of the highest quality to its readers.',
  ),
  'metaCitations' => false,
  'initials' => 
  array (
    'en_US' => 'JOTS',
  ),
  'printIssn' => '1071-6084',
  'onlineIssn' => '1541-9258',
  'mailingAddress' => '',
  'useEditorialBoard' => false,
  'contactName' => 'Marvin Sarapin',
  'contactTitle' => 
  array (
    'en_US' => 'Editor',
  ),
  'contactEmail' => 'jots@bdsu.edu',
  'contactPhone' => '',
  'contactFax' => '',
  'supportName' => 'Chase Dooley',
  'supportEmail' => 'chasehd@vt.edu',
  'supportPhone' => '',
  'emailSignature' => '________________________________________________________________________
The Journal of Technology Studies
http://ejournals.lib.vt.edu/JOTS',
  'remindForInvite' => false,
  'remindForSubmit' => false,
  'numDaysBeforeInviteReminder' => 0,
  'numDaysBeforeSubmitReminder' => 0,
  'rateReviewerOnQuality' => false,
  'restrictReviewerFileAccess' => false,
  'reviewerAccessKeysEnabled' => false,
  'showEnsuringLink' => false,
  'mailSubmissionsToReviewers' => false,
  'authorSelectsEditor' => false,
  'enableLockss' => false,
  'reviewerDatabaseLinks' => 
  array (
    0 => 
    array (
      'title' => '',
      'url' => '',
    ),
  ),
  'notifyAllAuthorsOnDecision' => false,
  'authorGuidelines' => 
  array (
    'en_US' => '<p>Articles must conform to the current edition of the <em>Publication Manual of the American Psychological Association.</em> All articles must be original, represent work of the named authors, not be under consideration elsewhere, and not be published elsewhere in English or any other language. Electronic submissions in either rich–text format or Microsoft Word formats are required. E–mail submissions should be sent to the editor at <a href="mailto:jots@bgsu.edu">jots@bgsu.edu</a>.</p> <p>Manuscripts should be no more that 25 double–spaced and unjustified pages, including references. Abstracts are required and should be no longer than 250 words. Also required is a list of keywords from your paper in your abstract. To do this, indent as you would if you were starting a new paragraph, type <em>keywords</em>: (italicized), and then list your keywords. Listing keywords will help researchers find your work in databases.</p> <p>Typescript should be 12 point <em>Times New Roman</em> or a close approximation. Only manuscripts in English that conform to American usage will be accepted. Figures, tables, photographs, and artwork must be of good quality and conform to the <em>Publication Manual of the American Psychological Association, specifically complying with the rules of Style<sup>®</sup></em> for form, citation style, and copyright. The Journal of Technology Studies seeks to maintain the highest standards of academic integrity and asks all contributors to apply proper due diligence in manuscript preparation.</p>',
  ),
  'submissionChecklist' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'order' => '1',
        'content' => 'The submission has not been previously published, nor is it before another journal for consideration (or an explanation has been provided in Comments to the Editor).',
      ),
      1 => 
      array (
        'order' => '2',
        'content' => 'The submission file is in OpenOffice, Microsoft Word, RTF, or WordPerfect document file format.',
      ),
      2 => 
      array (
        'order' => '3',
        'content' => 'Where available, URLs for the references have been provided.',
      ),
      3 => 
      array (
        'order' => '4',
        'content' => 'The text is single-spaced; uses a 12-point font; employs italics, rather than underlining (except with URL addresses); and all illustrations, figures, and tables are placed within the text at the appropriate points, rather than at the end.',
      ),
      4 => 
      array (
        'order' => '5',
        'content' => 'The text adheres to the stylistic and bibliographic requirements outlined in the <a href="/JOTS/about/submissions#authorGuidelines" target="_new">Author Guidelines</a>, which is found in About the Journal.',
      ),
      5 => 
      array (
        'order' => '6',
        'content' => 'If submitting to a peer-reviewed section of the journal, the instructions in <a href="javascript:openHelp(\'http://ejournals.lib.vt.edu/JOTS/help/view/editorial/topic/000044\')">Ensuring a Blind Review</a> have been followed.',
      ),
    ),
  ),
  'includeCreativeCommons' => false,
  'copyrightNoticeAgree' => false,
  'requireAuthorCompetingInterests' => false,
  'requireReviewerCompetingInterests' => false,
  'metaDiscipline' => false,
  'metaSubjectClass' => false,
  'metaSubjectClassUrl' => 
  array (
    'en_US' => 'http://',
  ),
  'metaSubject' => false,
  'metaCoverage' => false,
  'metaType' => false,
  'metaCitationOutputFilterId' => -1,
  'copySubmissionAckPrimaryContact' => false,
  'copySubmissionAckSpecified' => false,
  'copySubmissionAckAddress' => '',
  'disableUserReg' => false,
  'restrictSiteAccess' => false,
  'restrictArticleAccess' => false,
  'articleEventLog' => false,
  'articleEmailLog' => false,
  'publicationFormatVolume' => true,
  'publicationFormatNumber' => true,
  'publicationFormatYear' => true,
  'publicationFormatTitle' => true,
  'initialVolume' => 22,
  'initialNumber' => 2,
  'initialYear' => 1996,
  'useCopyeditors' => false,
  'useLayoutEditors' => false,
  'provideRefLinkInstructions' => false,
  'useProofreaders' => false,
  'copyeditInstructions' => 
  array (
    'en_US' => 'The copyediting stage is intended to improve the flow, clarity, grammar, wording, and formatting of the article. It represents the last chance for the author to make any substantial changes to the text because the next stage is restricted to typos and formatting corrections.   The file to be copyedited is in Word or .rtf format and therefore can easily be edited as a word processing document. The set of instructions displayed here proposes two approaches to copyediting. One is based on Microsoft Word\'s Track Changes feature and requires that the copy editor, editor, and author have access to this program. A second system, which is software independent, has been borrowed, with permission, from the Harvard Educational Review. The journal editor is in a position to modify these instructions, so suggestions can be made to improve the process for this journal.   <h4>Copyediting Systems</h4> <strong>1. Microsoft Word\'s Track Changes</strong> Under Tools in the menu bar, the feature Track Changes enables the copy editor to make insertions (text appears in color) and deletions (text appears crossed out in color or in the margins as deleted). The copy editor can posit queries to both the author (Author Queries) and to the editor (Editor Queries) by inserting these queries in square brackets. The copyedited version is then uploaded, and the editor is notified. The editor then reviews the text and notifies the author.  The editor and author should leave those changes with which they are satisfied. If further changes are necessary, the editor and author can make changes to the initial insertions or deletions, as well as make new insertions or deletions elsewhere in the text. Authors and editors should respond to each of the queries addressed to them, with responses placed inside the square brackets.   After the text has been reviewed by editor and author, the copy editor will make a final pass over the text accepting the changes in preparation for the layout and galley stage.   <strong>2. Harvard Educational Review </strong> <strong>Instructions for Making Electronic Revisions to the Manuscript</strong> Please follow the following protocol for making electronic revisions to your manuscript:  <strong>Responding to suggested changes.</strong> For each of the suggested changes that you accept, unbold the text.   For each of the suggested changes that you do not accept, re-enter the original text and <strong>bold</strong> it.  <strong>Making additions and deletions.</strong> Indicate additions by <strong>bolding</strong> the new text.   Replace deleted sections with: <strong>[deleted text]</strong>.   If you delete one or more sentence, please indicate with a note, e.g., <strong>[deleted 2 sentences]</strong>.  <strong>Responding to Queries to the Author (QAs).</strong> Keep all QAs intact and bolded within the text. Do not delete them.   To reply to a QA, add a comment after it. Comments should be delimited using: <strong>[Comment:]</strong> e.g., <strong>[Comment: Expanded discussion of methodology as you suggested]</strong>.  <strong>Making comments.</strong> Use comments to explain organizational changes or major revisions   e.g., <strong>[Comment: Moved the above paragraph from p. 5 to p. 7].</strong> Note: When referring to page numbers, please use the page numbers from the printed copy of the manuscript that was sent to you. This is important since page numbers may change as a document is revised electronically.  <h4>An Illustration of an Electronic Revision</h4> <ol> <li><strong>Initial copyedit.</strong> The journal copy editor will edit the text to improve flow, clarity, grammar, wording, and formatting, as well as including author queries as necessary. Once the initial edit is complete, the copy editor will upload the revised document through the journal Web site and notify the author that the edited manuscript is available for review.</li> <li><strong>Author copyedit.</strong> Before making dramatic departures from the structure and organization of the edited manuscript, authors must check in with the editors who are co-chairing the piece. Authors should accept/reject any changes made during the initial copyediting, as appropriate, and respond to all author queries. When finished with the revisions, authors should rename the file from AuthorNameQA.doc to AuthorNameQAR.doc (e.g., from LeeQA.doc to LeeQAR.doc) and upload the revised document through the journal Web site as directed.</li> <li><strong>Final copyedit.</strong> The journal copy editor will verify changes made by the author and incorporate the responses to the author queries to create a final manuscript. When finished, the copy editor will upload the final document through the journal Web site and alert the layout editor to complete formatting.</li> </ol>',
  ),
  'refLinkInstructions' => 
  array (
    'en_US' => '<h4>To Add Reference Linking to the Layout Process</h4> <p>When turning a submission into HTML or PDF, make sure that all hyperlinks in the submission are active.</p> <h4>A. When the Author Provides a Link with the Reference</h4> <ol> <li>While the submission is still in its word processing format (e.g., Word), add the phrase VIEW ITEM to the end of the reference that has a URL.</li> <li>Turn that phrase into a hyperlink by highlighting it and using Word\'s Insert Hyperlink tool and the URL prepared in #2.</li> </ol> <h4>B. Enabling Readers to Search Google Scholar For References</h4> <ol> <li>While the submission is still in its word processing format (e.g., Word), copy the title of the work referenced in the References list (if it appears to be too common a title—e.g., "Peace"—then copy author and title).</li> <li>Paste the reference\'s title between the %22\'s, placing a + between each word: http://scholar.google.com/scholar?q=%22PASTE+TITLE+HERE%22&amp;hl=en&amp;lr=&amp;btnG=Search.</li> <li>Add the phrase GS SEARCH to the end of each citation in the submission\'s References list.</li> <li>Turn that phrase into a hyperlink by highlighting it and using Word\'s Insert Hyperlink tool and the URL prepared in #2.</li> </ol> <h4>C. Enabling Readers to Search for References with a DOI</h4> <ol> <li>While the submission is still in Word, copy a batch of references into CrossRef Text Query http://www.crossref.org/freeTextQuery/.</li> <li>Paste each DOI that the Query provides in the following URL (between = and &amp;): http://www.cmaj.ca/cgi/external_ref?access_num=PASTE DOI#HERE&amp;link_type=DOI.</li> <li>Add the phrase CrossRef to the end of each citation in the submission\'s References list.</li> <li>Turn that phrase into a hyperlink by highlighting the phrase and using Word\'s Insert Hyperlink tool and the appropriate URL prepared in #2.</li> </ol>',
  ),
  'publishingMode' => 0,
  'showGalleyLinks' => false,
  'enableAnnouncements' => false,
  'enableAnnouncementsHomepage' => false,
  'numAnnouncementsHomepage' => 0,
  'volumePerYear' => 0,
  'issuePerVolume' => 0,
  'enablePublicIssueId' => false,
  'enablePublicArticleId' => false,
  'enablePublicGalleyId' => false,
  'enablePublicSuppFileId' => false,
  'enablePageNumber' => false,
  'customAboutItems' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'title' => 'Editorial Board',
        'content' => '<p>David Batts<br /> East Carolina University<br /> 230 Slay Building<br /> Greenville, NC 27858<br /> 252.328.9673<br /> <a href="mailto:battsd@ecu.edu">battsd@ecu.edu</a></p> <p>Karen Birch<br /> Representing the Board of Directors<br /> CT Community Colleges<br /> College of Technology/Regional<br /> Center for Next Generation<br /> Manufacturing<br /> 61 Woodland St.<br /> Hartford, CT 06105<br /> 860.244.7608</p> <p>Aaron C. Clark<br /> Department of Math, Science &amp;<br /> Technology Education<br /> North Carolina State University<br /> Box 7801, 510K Poe Hall<br /> Raleigh, NC 27695<br /> 919.515.1771<br /> <a href="mailto:aaron_clark@ncsu.edu">aaron_clark@ncsu.edu</a></p> <p>Jenny Daugherty<br /> Technology Leadership and Innovation<br /> Purdue University<br /> Ernest C Young Hall, Room 443<br /> West Lafayette, IN 47907<br /> 765.494.7989<br /> <a href="mailto:jldaughe@purdue.edu">jldaughe@purdue.edu</a></p> <p>Jeremy V. Ernst<br /> Department of Teaching and Learning<br /> Virginia Polytechnic Institute and State University<br /> 315 War Memorial Hall<br /> Blacksburg, VA 24061<br /> 540.231.2040<br /> <a href="mailto:jvernst@vt.edu">jvernst@vt.edu</a></p> <p>Ivan T. Mosley, Sr.<br /> 2 Pipers Glen Vt.<br /> Greensboro, NC 27406<br /> Phone: (336) 285-6468<br /> <a href="mailto:mosleyivant@gmail.com">mosleyivant@gmail.com</a></p> <p>Luke J. Steinke<br /> School of Technology<br /> Eastern Illinois University<br /> 600 Lincoln Avenue<br /> Charleston, IL 61920<br /> 217.581.6271<br /> <a href="mailto:lsteinke@eiu.edu">lsteinke@eiu.edu</a></p> <p>Shawn Strong<br /> Technology and Construction<br /> Management Department<br /> Missouri State University<br /> 901 S. National Rd.<br /> Springfield, MO 65897<br /> 417.836.5121<br /> <a href="mailto:shawnstrong@missouristate.edu">shawnstrong@missouristate.edu</a></p> <p>Jeffery M. Ulmer<br /> University of Central Missouri<br /> Grinstead 014A<br /> Warrensburg, MO 64093<br /> 600.827.2473<br /> <a href="mailto:julmer@ucmo.edu">julmer@ucmo.edu</a></p>',
      ),
    ),
  ),
  'proofInstructions' => 
  array (
    'en_US' => '<p>The proofreading stage is intended to catch any errors in the galley\'s spelling, grammar, and formatting. More substantial changes cannot be made at this stage, unless discussed with the Section Editor. In Layout, click on VIEW PROOF to see the HTML, PDF, and other available file formats used in publishing this item.</p> <h4>For Spelling and Grammar Errors</h4> <p>Copy the problem word or groups of words and paste them into the Proofreading Corrections box with "CHANGE-TO" instructions to the editor as follows:</p> <pre>1. CHANGE...
	then the others
	TO...
	than the others</pre> <br /> <pre>2. CHANGE...
	Malinowsky
	TO...
	Malinowski</pre> <br /> <h4>For Formatting Errors</h4> <p>Describe the location and nature of the problem in the Proofreading Corrections box after typing in the title "FORMATTING" as follows:</p> <br /> <pre>3. FORMATTING
	The numbers in Table 3 are not aligned in the third column.</pre> <br /> <pre>4. FORMATTING
	The paragraph that begins "This last topic..." is not indented.</pre>',
  ),
  'openAccessPolicy' => 
  array (
    'en_US' => 'This journal provides immediate open access to its content on the principle that making research freely available to the public supports a greater global exchange of knowledge. All issues (begining wih 1995 to the current year), both print and those published in electronic format, are available online.',
  ),
  'displayCurrentIssue' => false,
  'journalTheme' => '',
  'focusScopeDesc' => 
  array (
    'en_US' => '<p>JOTS welcomes <em>original</em> manuscripts from scholars worldwide, focused on the depth and breadth of technology as practiced and understood past, present, and future. Epsilon Pi Tau, as perhaps the most comprehensive honor society among technology professions, seeks to provide up-to-date and insightful information to its increasingly diverse membership as well as the broader public. Authors need not be members of the society in order to submit manuscripts for consideration. Contributions from academe, government, and the private sector are equally welcome.</p> <p>An overview of the breadth of topics of potential interest to our readers can be gained from the 17 subclasses within the "Technology" category in the Library of Congress classification scheme (<a href="http://www.loc.gov/catdir/cpso/lcco/lcco_t.pdf">http://www.loc.gov/catdir/cpso/lcco/lcco_t.pdf</a>). Authors are strongly urged to peruse this list as they consider developing articles for journal consideration. In addition, JOTS is interested in manuscripts that provide:</p> <ul class="home"> <li>brief biographical portraits of leaders in technology that highlight the individuals’ contributions made in dis tinct fields of technology or its wider appreciation within society,</li> <li>thoughtful reflections about technology practice,</li> <li>insights about personal transitions in technology from formal education to the work environment or vice versa,</li> <li>anthropology, economics, history, philosophy, and sociology of technology,</li> <li>technology within society and its relationship to other disciplines,</li> <li>technology policy at local, national, and international levels,</li> <li>comparative studies of technology development, implementation, and/or education,</li> <li>industrial research and development,or</li> <li>new and emerging technologies and technology’s role in shaping the future.</li> </ul>',
  ),
  'reviewPolicy' => 
  array (
    'en_US' => '<p>Articles deemed worthy for consideration by the editor undergo anonymous peer review by members of the JOTS editorial board. Authors who submit an article that does not merit review by the editorial board are informed within approximately three weeks of receipt of the article so they may explore other publishing venues. A rejection may be based solely on the content focus of the article and not its intrinsic merit, particularly where the topic has been extensively explored in prior JOTS articles. Articles that exhibit extensive problems in expression, grammar, spelling, and/or APA format are summarily rejected. Authors of articles that have been peer-reviewed are informed within three months from the date of submission. Anonymous comments of reviewers are provided to authors who are invited to submit a revised article for either publication or a second round of review. The editor does not automatically provide reviewer comments to authors whose articles have been rejected via the peer review process. However, such feedback may be provided if the editor determines that the feedback might prove helpful to authors as they pursue other publishing opportunities.</p>',
  ),
  'privacyStatement' => 
  array (
    'en_US' => 'The names and email addresses entered in this journal site will be used exclusively for the stated purposes of this journal and will not be made available for any other purpose or to any other party.',
  ),
  'lockssLicense' => 
  array (
    'en_US' => 'This journal utilizes the LOCKSS system to create a distributed archiving system among participating libraries and permits those libraries to create permanent archives of the journal for purposes of preservation and restoration. <a href="http://lockss.org/">More...</a>',
  ),
  'readerInformation' => 
  array (
    'en_US' => 'We encourage readers to sign up for the publishing notification service for this journal. Use the <a href="/JOTS/user/register">Register</a> link at the top of the home page for the journal. This registration will result in the reader receiving the Table of Contents by email for each new issue of the journal. This list also allows the journal to claim a certain level of support or readership. See the journal\'s <a href="/JOTS/about/submissions#privacyStatement">Privacy Statement</a>, which assures readers that their name and email address will not be used for other purposes.',
  ),
  'authorInformation' => 
  array (
    'en_US' => 'Interested in submitting to this journal? We recommend that you review the <a href="/JOTS/about">About the Journal</a> page for the journal\'s section policies, as well as the <a href="/JOTS/about/submissions#authorGuidelines">Author Guidelines</a>. Authors need to <a href="/JOTS/user/register">register</a> with the journal prior to submitting or, if already registered, can simply <a href="/index/login">log in</a> and begin the five-step process.',
  ),
  'librarianInformation' => 
  array (
    'en_US' => 'We encourage research librarians to list this journal among their library\'s electronic journal holdings. As well, it may be worth noting that this journal\'s open source publishing system is suitable for libraries to host for their faculty members to use with journals they are involved in editing (see <a href="http://pkp.sfu.ca/ojs">Open Journal Systems</a>).',
  ),
  'description' => 
  array (
    'en_US' => '<p><em>The Journal of Technology Studies</em> (JOTS) is the flagship, peer–reviewed journal of Epsilon Pi Tau, an international honor society for technology professions. One printed volume per year is mailed to all active members of the society as well as to subscribing academic and general libraries around the globe.</p><p>JOTS welcomes <em>original</em> manuscripts from scholars worldwide, focused on the depth and breadth of technology as practiced and understood past, present, and future. Epsilon Pi Tau, as perhaps the most comprehensive honor society among technology professions, seeks to provide up-to-date and insightful information to its increasingly diverse membership as well as the broader public. Authors need not be members of the society in order to submit manuscripts for consideration. Contributions from academe, government, and the private sector are equally welcome.</p><p>An overview of the breadth of topics of potential interest to our readers can be gained from the 17 subclasses within the "Technology" category in the Library of Congress classification scheme (<a href="http://www.loc.gov/catdir/cpso/lcco/lcco_t.pdf">http://www.loc.gov/catdir/cpso/lcco/lcco_t.pdf</a>). Authors are strongly urged to peruse this list as they consider developing articles for journal consideration. In addition, JOTS is interested in manuscripts that provide:</p><ul class="home"><li>brief biographical portraits of leaders in technology that highlight the individuals’ contributions made in dis tinct fields of technology or its wider appreciation within society,</li><li>thoughtful reflections about technology practice,</li><li>insights about personal transitions in technology from formal education to the work environment or vice versa,</li><li>anthropology, economics, history, philosophy, and sociology of technology,</li><li>technology within society and its relationship to other disciplines,</li><li>technology policy at local, national, and international levels,</li><li>comparative studies of technology development, implementation, and/or education,</li><li>industrial research and development,or</li><li>new and emerging technologies and technology’s role in shaping the future.</li></ul>',
  ),
  'navItems' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'name' => '',
        'url' => '',
      ),
    ),
  ),
); ?>